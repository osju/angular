// este es una clase plana para guardar la nueva variable
// se crea una carpeta dentro de app llamada models donde se guarda classes de modelo q surgen logica de nogocio
// se llama destino-DestinoViajes se define un objeto

export class Nombres {
    //se usa servicios para mejorar el template un array de strings

    public servicios: string[];
    private selected: boolean = false;


    constructor( public nombre: string, public imagenUrl:string){
    //emitir evenotos entre componentes. se evita el seteo al usar PUBLIC
    this.servicios = ['pileta', 'desayuno']; //se debe colocar en la vista de componente saludo..
    }
     isSelected(): boolean{
         return this.selected;
    }
    setSelected(s: boolean){
        this.selected = s;
    }
}

