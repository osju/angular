import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
// para el ruteo se debe importar la libreiria

import{RouterModule, Routes} from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SaludoComponent } from './saludo/saludo.component';
import { NombresComponent } from './nombres/nombres.component';
import { DestinoComponent } from './destino/destino.component';
//formularios interactivos
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormSaludoComponent } from './form-saludo/form-saludo.component';
// FormDestinoViajeComponent == formsauldocomponent

// se defineen las rutas 
const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'home', component: NombresComponent},
  {path: 'destino', component: DestinoComponent},
  // para cargarlas se debe agregar a los imports del modulo

]

@NgModule({
  declarations: [
    AppComponent,
    SaludoComponent,
    NombresComponent,
    DestinoComponent,
    FormSaludoComponent
  
  ],
  imports: [
    RouterModule.forRoot(routes),
    AppRoutingModule,
    BrowserModule,
    // Para manejo de FORMULARIOS interactivostivos se crea comonentete 
    // se crea from-saludo 
    FormsModule,
    ReactiveFormsModule
    
    // se registran las rutas al modulo
    
  ],
  exports: [RouterModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
