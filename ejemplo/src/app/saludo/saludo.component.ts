import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { Nombres } from '../models/nombres.model';

@Component({
  selector: 'app-saludo',
  templateUrl: './saludo.component.html',
  styleUrls: ['./saludo.component.css']
})
export class SaludoComponent implements OnInit {
  @Input() position!: number;
  @Input() nombreF!:Nombres;
  // @HostBinding este se encarga de aplicar al atributo class del html que genera el componten cuando se renderea 
  // la variable cssClass que contiene col-md-4 
  @HostBinding('attr.class') cssClass = ' col-md-4 padd';
  // se declara la propiedad clicke de tipo eventemmiter 
  // se declara la propiedad clicke de tipo eventemmiter 
  @Output() clicked: EventEmitter<Nombres>;
  constructor() {
    // se inicializa la variable clicked se carga un objeto a la propiedad para usarla se va a la lista es desicir nombres
    this.clicked = new EventEmitter;
   }
  //  se define el emtidor de evento se emite al componente padre cual de los nombres fue seleccionado 
   ir(){
     this.clicked.emit(this.nombreF);
     return false; //para que no recarge pagina
   }

  ngOnInit(): void {
  }

}
