import { stringify } from '@angular/compiler/src/util';
import { Component, OnInit } from '@angular/core';
import { ɵangular_packages_core_testing_testing_a } from '@angular/core/testing';
import { Nombres } from '../models/nombres.model';

@Component({
  selector: 'app-nombres',
  templateUrl: './nombres.component.html',
  styleUrls: ['./nombres.component.css']
})
export class NombresComponent implements OnInit {
  // nombresF es una variable que contiene array de Nombres que contien nnombres y imgurl
  nombresF!: Nombres[];
   constructor() { 
    //  se inicializa variable vacia
  this.nombresF=[];
   }

  ngOnInit(): void {
  }
  // la funcion se la define aqui recibe dos parametros pero debe retornar un valor cuando es un evento retorna false para no recargar la pagina
  guardar(nombre:string, url:string):boolean{
    // el push. inyecta o agrega un elemento 
    this.nombresF.push(new Nombres(nombre, url));
    //console.log(new Nombres(nombre,url));
    console.log(this.nombresF);
    // Nombre es el objteto que lo creamos en model 
    //console.log(url);
    return false;
  }
  elegido(item: Nombres){
    this.nombresF.forEach(function(x) {
      x.setSelected(false);}) //se desmarca a los otros items como preferido 
      // se marca al seleccionado como preeferido
      item.setSelected(true);
  }
}
