import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormSaludoComponent } from './form-saludo.component';

describe('FormSaludoComponent', () => {
  let component: FormSaludoComponent;
  let fixture: ComponentFixture<FormSaludoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormSaludoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormSaludoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
